# Node AMQP 1.0 Client Module
## Introduction
A module for connecting to AMQP 1.0 messaging queues such as AWS MQ. Allows for unicast/broadcast messages and leader election if multiple clients are connected.

## Setup
### Installing the Module
```
npm install --save amqp-1.0-client
```
### Enviroment Variables
None of the below fiels are strictly required and will revert to default if not set. Please note that depending on your setup the defaults may or may not work.
| Name | Description | Required | Default | Options | 
| - | - | :-: | :-: | :-: | 
| **AMQP_HOST** | Hostname of the AMQP endpoint. | false | localhost | / | 
| **AMQP_PORT** | Port of the AMQP endpoint. | false | 5672 | 5672/5671 (ssl) | 
| **AMQP_PROTO** | Protocol used by the AMQP endpoint. | false | amqp | amqp/amqps (ssl) | 
| **AMQP_USERNAME** | Username for secure connections. | false | admin | / | 
| **AMQP_PASSWORD** | Password for secure connections. | false | admin | / | 
| **AMQP_CLIENT_POLL_INTERVAL** | Update frquency used to publish/check for cluster clients | false | 10(seconds) | 10 to 60 (recommended) | 
| **AMQP_CLIENT_EXPIRE_LIMIT** | Known clients list, set the maximum time before deleting them since they are last seen. | false | 30 | 30 to 120 (recommended) || **AMQP_MESSAGE_EXPIRE_LIMIT** | Length of time to keep the same received message in memory before deleting it. | false | 120 | 60 to 120 (recommended) | 
| **AMQP_MESSAGE_SEND_INTERVAL** | Interval to send messages (recommended to be short). | false | 1 | 1 to 60 (recommended) | 
| **AMPQ_DEBUG_ENABLED** | If set to true, debug messages for the AMQP client will be printed. | false | false | / | 

## Usage
Below are a few simple examples on how to use this module. It's intended to simplify the process of sending ad processing messages.
```
// Initialize the module.
const amqp = require("amqp-1.0-client");

// Connect to a queue and assign action to new messages.
amqp.connect("MY_QUEUE", process_task);

var list_of_tasks = [
    "one",
    "two",
    "three"
];

// On ready send a broadcast message and single message to a random endpoint.
amqp.on("ready", () => {
    amqp.broadcast("This is a broadcast");
    amqp.send("This is a unicast message!");

    // If this is the leader then sed out tasks to everyone.
    if (amqp.leader) {
        for (var task of list_of_tasks){
            amqp.send({
                "task": task,
                "some_lbl": "some value"
            });
        }
    }
});

// Perform an action when a message is received. Note that the internal stricture of the "message" variable is entirely at your discression.
const process_task = (message) => {
    console.log(`Task received: ${message.task}`)
}

// Do something if the leader changes.
amqp.on("leader_change", () => {
    console.log("All hail the new leader!");
});
```

## To-Do
- Add method for setting module variables using a module function or variables rather than enviroment variables.
- Add support for amqp 0.9.
- Suggestions? Submit an issue on the project page on GitLab!