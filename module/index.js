// Imports.
const AMQP = require('amqp10').Client;
const uuidv4 = require('uuid/v4');
const env = require('./env');
const log = require('./log');

const urienc = (itemStr) => {
  return encodeURIComponent(itemStr);
};

// AMQP variables.
const AMQP_HOST = env.get('AMQP_HOST', 'localhost');
const AMQP_PORT = env.get('AMQP_PORT', '5672');
const AMQP_PROTO = env.get('AMQP_PROTO', 'amqp');
const AMQP_USERNAME = env.get('AMQP_USERNAME', 'admin');
const AMQP_PASSWORD = env.get('AMQP_PASSWORD', 'admin');
const AMQP_CLIENT_POLL_INTERVAL = env.get('AMQP_CLIENT_POLL_INTERVAL', 10) * 1000;
const AMQP_CLIENT_EXPIRE_LIMIT = env.get('AMQP_CLIENT_EXPIRE_LIMIT', 60) * 1000;
const AMQP_MESSAGE_SEND_INTERVAL = env.get('AMQP_MESSAGE_SEND_INTERVAL', 1) * 1000;
const AMQP_URL = `${AMQP_PROTO}://${urienc(AMQP_USERNAME)}:${urienc(AMQP_PASSWORD)}@${urienc(AMQP_HOST)}:${AMQP_PORT}`;

log.write('AMQP Module initialization in progress...', 'WARN');
log.write(`Connecting to host '${AMQP_HOST}'.`, 'WARN');

// Base cluster related variables.
const clientID = `${uuidv4()}`;
const leaderSeed = Date.now();
let isLeader = false;
let leaderID = null;
let isLeaderElected = false;
let leaderChangeCallback = null;

log.write(`Client: '${clientID}'.`, 'WARN');
log.write(`Seed:   '${leaderSeed}'.`, 'WARN');

// Stored messages.
const messageStoreIncoming = {};
const messageStoreOutgoing = {};
const clientStore = {};

const evaluateMessages = () => {
  // TO-DO.
};

// Evaluate who should be the leader.
const evaluateLeader = () => {
  if (Object.keys(clientStore).length > 0) {
    if (leaderSeed + (AMQP_CLIENT_EXPIRE_LIMIT) > Date.now()) {
      log.write('Waiting for deployment to settle before electing a leader.', 'WARN');
      return;
    }
    let leader = clientStore[Object.keys(clientStore)[0]];
    let inspectClientID = null;
    for (inspectClientID of Object.keys(clientStore)) {
      if (leader.leaderSeed > clientStore[inspectClientID].leaderSeed) {
        leader = clientStore[inspectClientID];
      }
    }
    for (inspectClientID of Object.keys(clientStore)) {
      clientStore[inspectClientID].leader = (leader.clientID === inspectClientID);
    }
    isLeader = (leader.clientID === clientID);
    if (leader.clientID != leaderID) {
      if (leaderChangeCallback) {
        leaderChangeCallback();
      }
      isLeaderElected = true;
      log.write(`New leader elected: '${leader.clientID}'`, 'WARN');
      if (isLeader) {
        log.write(`This client is now the leader.`, 'WARN');
      }
    }
    leaderID = leader.clientID;
  }
};

// Check if client 'last_seen' parameter is within set values.
const clientValid = (client) => {
  return (client.last_seen + AMQP_CLIENT_POLL_INTERVAL + AMQP_CLIENT_EXPIRE_LIMIT) > Date.now();
};

// If client is valid, do nothing, if it has expired, remove it from the list.
const evaluateClients = () => {
  if (Object.keys(clientStore).length) {
    for (const clientID of Object.keys(clientStore)) {
      if (!clientValid(clientStore[clientID])) {
        log.write(`Deleting client '${clientID}', timeout exceeded.`, 'WARN');
        delete clientStore[clientID];
      }
    }
    evaluateLeader();
  }
  setTimeout(evaluateClients, AMQP_CLIENT_POLL_INTERVAL);
};

// Initialize AMQP client from 'amqp10'.
const client = new AMQP();

// Default log actions.
const logMessage = (message) => {
  log.write(message);
};
const logWarning = (message) => {
  log.write(message, 'WARN');
};
const logError = (message) => {
  log.write(message, 'ERR');
};

// Build standardised message object.
const buildMessage = (message, category = 'unicast') => {
  return {
    'client': clientID,
    'content': message,
    'message_id': `${uuidv4()}`,
    'sent_timestamp': `${Date.now()}`,
    'category': category,
    'seen_by': [],
  };
};

// Get message content.
const decodeMessage = (message) => {
  return message.content;
};

// Component variables for the amqp10 module.
let amqpSender = null;

// Process messages marked with the 'system' category.
const processSystemEvent = (message) => {
  message.last_seen = Date.now();
  clientStore[message.clientID] = message;
  evaluateLeader();
};

// Send a broadcast message to all clients for clustering purposes.
const broadcastSystemEvent = () => {
  sendmessageObject({
    'category': 'system',
    'clientID': clientID,
    'leaderSeed': leaderSeed,
    'message_id': uuidv4(),
  });
  setTimeout(broadcastSystemEvent, AMQP_CLIENT_POLL_INTERVAL);
};

// Check if the message needs to be sent further.
const broadcastRequired = (message) => {
  let missing = false;
  for (const client of Object.keys(clientStore)) {
    if (!message.seen_by.includes(client)) {
      missing = true;
    }
  }
  return missing;
};

const getMessage = (id) => {
  return messageStoreIncoming[id];
};

// Process message and perform adequate task.
const processMessage = (message, messageAction) => {
  message = message.body;
  if (!getMessage(message.message_id) || message.category == 'broadcast') {
    messageStoreIncoming[message.message_id] = message;
    switch (message.category) {
      case 'system':
        processSystemEvent(message);
        sendmessageObject(message);
        break;
      case 'broadcast':
        if (!message.seen_by.includes(clientID)) {
          message.seen_by.push(clientID);
          messageAction(decodeMessage(message));
        }
        if (broadcastRequired(message)) {
          sendmessageObject(message);
        } else {
          log.write(`Broadcast complete for ${message.message_id}.`, 'WARN');
        }
        break;
      case 'unicast':
        messageAction(decodeMessage(message));
        break;
      default:
        logWarning(`Skip ${message.body.message_id}: no category specified.`);
    }
  }
};

// Initialize connection with paramaters passed and enviroment variables.
const initConnection = (topic, messageAction, errorAction) => {
  client.connect(AMQP_URL)
      .then(() => {
        log.write('Connection established.', 'WARN');
        return Promise.all([
          client.createReceiver(`amq.${topic}`),
          client.createSender(`amq.${topic}`),
        ]);
      })
      .spread((receiver, sender) => {
        receiver.on('errorReceived', (err) => {
          errorAction(err);
        });
        receiver.on('message', (message) => {
          processMessage(message, messageAction);
        });
        amqpSender = sender;
        log.write('Component setup complete.', 'WARN');
        evaluateMessages();
        evaluateClients();
        broadcastSystemEvent();
        processOutgoingQueue();
      })
      .error((err) => {
        errorAction(err);
      });
};

// Wrapper for direct message object send.
const sendOutgoingMsg = (messageObject) => {
  amqpSender.send(messageObject);
};

// All messages are cached before being sent, this will flush all messages.
const processOutgoingQueue = () => {
  if (Object.keys(messageStoreOutgoing)) {
    for (const id of Object.keys(messageStoreOutgoing)) {
      if (amqpSender) {
        try {
          sendOutgoingMsg(messageStoreOutgoing[id]);
          delete messageStoreOutgoing[id];
        } catch (err) {
          log.write(`Failed to send message ${id}.`, 'ERR');
        }
      }
    }
  }
  setTimeout(processOutgoingQueue, AMQP_MESSAGE_SEND_INTERVAL);
};

const sendmessageObject = (messageObject) => {
  messageStoreOutgoing[messageObject.message_id] = messageObject;
};
const sendMessage = (text, category = 'unicast') => {
  const message = buildMessage(text, category);
  messageStoreOutgoing[message.message_id] = message;
};

let readyCallback = null;
const isReady = () => {
  isLeaderElected ? readyCallback() : setTimeout(isReady, 500);
};

module.exports = {
  /**
   * @return {boolean} leader - Boolean to indicate if the client is the elected leader of the cluster.
  */
  leader: isLeader,
  /**
  * @return {object} activeClients - Object containing all cluster clients.
  */
  activeClients: clientStore,
  /**
  * @return {array} activeClientIDs - Array containing all cluster client ids.
  */
  activeClientIDs: Object.keys(clientStore),
  /**
     * Connect to the AMQP queue and bind to a specific topic.
     * @param {string} topic - String indicating the topic that this client should bind to.
     * @param {function} messageAction(message) - Function to be called when the client receives a message.
     * @param {function} errorAction(error) - Function to be called when there is an error.
  */
  connect: (topic, messageAction = logMessage, errorAction = logError) => {
    initConnection(topic, messageAction, errorAction);
  },
  /**
     * Send a message.
     * @param {any} content - Content of the message.
  */
  send: (content) => {
    sendMessage(content);
  },
  /**
     * Broadcast a message.
     * @param {any} content - Content of the message.
  */
  broadcast: (content) => {
    sendMessage(content, 'broadcast');
  },
  /**
     * Event listeners.
     * @param {string} state - State that the listener should attach to, can be 'ready' or 'leader_change'.
     * @param {function} callback - Callback function that should be called when the event occurs.
     * 
     * 'ready': Called when the client and clustering modules are ready, 
     * it's recommended to wait for ths to complete before using the client.
     * 
     * 'leader_change': Called when a new leader is elected. Usefult in combination with the "leader"
     * variable to determine if the current client should now be performing leader tasks.
  */
  on: (state, callback) => {
    switch (state) {
      case 'ready':
        readyCallback = callback;
        isReady();
        break;
      case 'leader_change':
        leaderChangeCallback = callback;
        break;
      default:
        callback();
    }
  },
};
